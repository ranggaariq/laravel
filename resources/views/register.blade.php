<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
    @csrf
        <label for="fname">First Name :</label> <br>
        <input type="text" name="fname" id="fname" required> <br> <br>

        <label for="lname">Last Name :</label> <br>
        <input type="text" name="lname" id="lname" require> <br> <br>

        <label for="gender">Gender :</label> <br>
        <input type="radio" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" value="female">Female
        <label for="female">Female</label><br>
        <input type="radio" name="gender">
        <label for="gender">Other</label>
        <input type="text" name="gender"><br> <br>

        <label for="national">Nationality :</label> 
        <select name="national" id="national">
            <option value="id">Indonesian</option>
            <option value="sg">Singaporean</option>
            <option value="my">Malaysian</option>
            <option value="au">Australian</option>
        </select> <br> <br>

        <label>Language Spoken :</label> <br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br> <br>

        <label>Bio : </label> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>