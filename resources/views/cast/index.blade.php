@extends('layouts.master-card')

@section('title')
   List Data Cast
@endsection


{{-- @section('content') --}}
   
@section('card-title')
    Tambah Data <a href="/cast/create" class="btn btn-primary ml-2">Tambah</a>
@endsection

@section('card-body')
<table class="table">
   <thead class="thead-light">
     <tr>
       <th scope="col">ID</th>
       <th scope="col">Nama</th>
       <th scope="col">Umur</th>
       <th scope="col">Bio</th>
       <th scope="col">Action</th>
     </tr>
   </thead>
   <tbody>
       @forelse ($cast as $key=>$value)
           <tr>
               <td>{{$key + 1}}</th>
               <td>{{$value->nama}}</td>
               <td>{{$value->umur}}</td>
               <td>{{$value->bio}}</td>
               <td>
                  <form action="/cast/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                       <input type="submit" class="btn btn-danger my-1" value="Delete">
                   </form>
               </td>
           </tr>
       @empty
           <tr colspan="3">
               <td>No data</td>
           </tr>  
       @endforelse              
   </tbody>
</table>
@endsection

@section('sidebar')
<li class="nav-item">
   <a href="/" class="nav-link">
     <i class="nav-icon fas fa-tachometer-alt"></i>
     <p>
       Dashboard
     </p>
   </a>
 </li>
 <li class="nav-item">
   <a href="#" class="nav-link">
     <i class="nav-icon fas fa-table"></i>
     <p>
       Tables
       <i class="fas fa-angle-left right"></i>
     </p>
   </a>
   <ul class="nav nav-treeview">
      <li class="nav-item">
        <a href="/table" class="nav-link">
          <i class="far fa-circle nav-icon"></i>
          <p>Table</p>
        </a>
      </li>
    </ul>
   <ul class="nav nav-treeview">
     <li class="nav-item">
       <a href="/data-table" class="nav-link">
         <i class="far fa-circle nav-icon"></i>
         <p>Data Table</p>
       </a>
     </li>
   </ul>
</li>
<li class="nav-item menu-open">
   <a href="#" class="nav-link">
     <i class="nav-icon far fa-plus-square"></i>
     <p>
       Movie
       <i class="fas fa-angle-left right"></i>
     </p>
   </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="/cast" class="nav-link active">
        <i class="far fa-circle nav-icon"></i>
        <p>Cast</p>
      </a>
    </li>
  </ul>
</li>
@endsection