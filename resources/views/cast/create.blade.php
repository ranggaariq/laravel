@extends('layouts.master')

@section('title')
    Tambah Data Cast
@endsection

@section('content')
<div class="card">
   <div class="card-header">
      <h2 class="card-title">Tambah Data Cast</h2>
      <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
           <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
           <i class="fas fa-times"></i>
         </button>
       </div>
   </div>

   <div class="card-body">
      <form action="/cast" method="POST">
         @csrf
         <div class="form-group">
               <label for="nama">Nama</label>
               <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
               @error('nama')
                  <div class="alert alert-danger">
                     {{ $message }}
                  </div>
               @enderror
         </div>
         <div class="form-group">
               <label for="umur">Umur</label>
               <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
               @error('umur')
                  <div class="alert alert-danger">
                     {{ $message }}
                  </div>
               @enderror
         </div>
         <div class="form-group">
               <label for="bio">Bio</label>
               <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Bio">
               @error('bio')
                  <div class="alert alert-danger">
                     {{ $message }}
                  </div>
               @enderror
         </div>
         <button type="submit" class="btn btn-primary">Tambah</button>
      </form>
   </div>
</div>
@endsection

@section('sidebar')
<li class="nav-item">
   <a href="#" class="nav-link">
     <i class="nav-icon fas fa-tachometer-alt"></i>
     <p>
       Dashboard
     </p>
   </a>
 </li>
 <li class="nav-item">
   <a href="#" class="nav-link">
     <i class="nav-icon fas fa-table"></i>
     <p>
       Tables
       <i class="fas fa-angle-left right"></i>
     </p>
   </a>
   <ul class="nav nav-treeview">
     <li class="nav-item">
       <a href="/data-table" class="nav-link">
         <i class="far fa-circle nav-icon"></i>
         <p>Data Table</p>
       </a>
     </li>
   </ul>
</li>
<li class="nav-item">
  <a href="#" class="nav-link">
    <i class="nav-icon fas fa-film"></i>
    <p>
      Movie
      <i class="fas fa-angle-left right"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="/cast" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Cast</p>
      </a>
    </li>
  </ul>
</li>
@endsection