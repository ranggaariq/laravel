@extends('layouts.master-card')

@section('title')
    Edit Data Cast
@endsection

@section('card-title')
Edit Cast : {{$cast->nama}}
    
@endsection

@section('card-body')
      <form action="/cast/{{$cast->id}}" method="POST">
         @csrf
         @method('put')
         <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" value="{{$cast->nama}}" placeholder="Masukkan Nama">
            @error('nama')
               <div class="alert alert-danger">
                  {{ $message }}
               </div>
            @enderror
         </div>
         <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" name="umur" id="umur" value="{{$cast->umur}}" placeholder="Masukkan Umur">
            @error('umur')
               <div class="alert alert-danger">
                  {{ $message }}
               </div>
            @enderror
         </div>
         <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" name="bio" id="bio" value="{{$cast->bio}}" placeholder="Masukkan Bio">
            @error('bio')
               <div class="alert alert-danger">
                  {{ $message }}
               </div>
            @enderror
         </div>
         <button type="submit" class="btn btn-primary">Edit</button>
      </form>

@endsection
@section('sidebar')
<li class="nav-item">
   <a href="#" class="nav-link">
     <i class="nav-icon fas fa-tachometer-alt"></i>
     <p>
       Dashboard
     </p>
   </a>
 </li>
 <li class="nav-item">
   <a href="#" class="nav-link">
     <i class="nav-icon fas fa-table"></i>
     <p>
       Tables
       <i class="fas fa-angle-left right"></i>
     </p>
   </a>
   <ul class="nav nav-treeview">
     <li class="nav-item">
       <a href="/data-table" class="nav-link">
         <i class="far fa-circle nav-icon"></i>
         <p>Data Table</p>
       </a>
     </li>
   </ul>
</li>
<li class="nav-item">
  <a href="#" class="nav-link">
    <i class="nav-icon fas fa-film"></i>
    <p>
      Movie
      <i class="fas fa-angle-left right"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="/cast" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Cast</p>
      </a>
    </li>
  </ul>
</li>
@endsection