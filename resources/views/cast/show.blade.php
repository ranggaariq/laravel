@extends('layouts.master')

@section('title')
    Show ID {{$cast->id}}
@endsection

@section('content')
   <h2>Show ID Cast ke-{{$cast->id}}</h2>
   <h4>Nama : {{$cast->nama}}</h4>
   <h4>Usia : {{$cast->umur}}</h4>
   <h4>Peran : {{$cast->bio}}</h4>
@endsection