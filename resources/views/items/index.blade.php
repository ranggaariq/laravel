@extends('layouts.master')

@section('title')
    Halaman Utama
@endsection


@section('content')
<div class="card">
   <div class="card-header">
      <h3 class="card-title">Halaman Utama</h3>
      <div class="card-tools">
         <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
           <i class="fas fa-minus"></i>
         </button>
         <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
           <i class="fas fa-times"></i>
         </button>
       </div>
   </div>

   <div class="card-body">
      Halaman Utama <a href="/register">link pendaftaran</a>
   </div>
</div>
@endsection

@section('sidebar')
<li class="nav-item">
   <a href="#" class="nav-link">
     <i class="nav-icon fas fa-tachometer-alt"></i>
     <p>
       Dashboard
     </p>
   </a>
 </li>
 <li class="nav-item">
   <a href="#" class="nav-link">
     <i class="nav-icon fas fa-table"></i>
     <p>
       Tables
       <i class="fas fa-angle-left right"></i>
     </p>
   </a>
   <ul class="nav nav-treeview">
     <li class="nav-item">
       <a href="/data-table" class="nav-link">
         <i class="far fa-circle nav-icon"></i>
         <p>Data Table</p>
       </a>
     </li>
   </ul>
</li>
<li class="nav-item">
  <a href="#" class="nav-link">
    <i class="nav-icon fas fa-film"></i>
    <p>
      Movie
      <i class="fas fa-angle-left right"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="/cast" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Cast</p>
      </a>
    </li>
  </ul>
</li>
@endsection